#!/bin/bash
cat /usr/share/zabbix/mysql/schema.sql | mysql -h mysql -uzabbix -pzabbix zabbix
cat /usr/share/zabbix/mysql/images.sql | mysql -h mysql -uzabbix -pzabbix zabbix
cat /usr/share/zabbix/mysql/data.sql | mysql -h mysql -uzabbix -pzabbix zabbix
/usr/sbin/zabbix_server_mysql -f
