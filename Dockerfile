FROM centos:latest
LABEL maintainer="Stephan Meystre <stephan.meystre@bkw.ch>"
LABEL io.k8s.description="Zabbix Server" \
  io.k8s.display-name="Zabbix Server 3.4" \
  io.openshift.expose-services="10051:10051" \
  io.openshift.tags="builder,zabbix"
  
ADD root /

RUN rpm -ivh http://repo.zabbix.com/zabbix/3.4/rhel/7/x86_64/zabbix-release-3.4-2.el7.noarch.rpm

RUN yum -y install zabbix-server-mysql zabbix-agent mysql && \
    yum clean all

RUN /usr/bin/fix-permissions /var/log/zabbix && \
    /usr/bin/fix-permissions /var/run/zabbix && \
    /usr/bin/fix-permissions /etc/zabbix

COPY ./entrypoint.sh /
COPY ./conf/zabbix_server.conf /etc/zabbix/zabbix_server.conf

EXPOSE 10051

ENTRYPOINT [ "/entrypoint.sh" ]
